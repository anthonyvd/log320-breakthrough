package breakthrough;

public interface IArtificialIntelligence {

	public abstract void OnInitialGridReceived(Grid grid, boolean opponentStarting, AbstractCommunicator sender);// true if second player , false if first

	public abstract void OnMoveReceived(Move opponentMove, AbstractCommunicator sender);

}