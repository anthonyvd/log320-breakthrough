package breakthrough;

public class MoveGenerator {
	private Grid grid;
	private boolean white;
	
	private int currentX = 0;
	private int currentY = 0;
	private int currentAttempts = 0;
	
	public MoveGenerator(Grid g, boolean white) {
		grid = g;
		this.white = white;
	}
	
	public Move nextValidMove() {
		while(currentAttempts == 3 ||
				(white ? 
				grid.tileContentAt(currentX, currentY) != Grid.TileContent.WhitePawn : 
				grid.tileContentAt(currentX, currentY) != Grid.TileContent.BlackPawn)) {
			goNextTile();
			if(currentY == 8) {
				return null;
			}
		}
		
		if(white) {
			if(currentAttempts == 0) { //try left diagonal
				++currentAttempts;
				if(currentX > 0 && currentY < 7 && grid.tileContentAt(currentX - 1, currentY + 1) != Grid.TileContent.WhitePawn) {
					return new Move(currentX, currentY, currentX - 1, currentY + 1, true);
				}
			} 
			if(currentAttempts == 1) { //Try forward
				++currentAttempts;
				if(currentY < 7 && grid.tileContentAt(currentX, currentY + 1) == Grid.TileContent.Nothing) {
					return new Move(currentX, currentY, currentX, currentY + 1, true);
				}
			} 
			if(currentAttempts == 2){ //Try right diagonal
				++currentAttempts;
				if(currentX < 7 && currentY < 7 && grid.tileContentAt(currentX + 1, currentY + 1) != Grid.TileContent.WhitePawn) {
					return new Move(currentX, currentY, currentX + 1, currentY + 1, true);
				}
			}
			return nextValidMove();
		} else {
			if(currentAttempts == 0) { //try left diagonal
				++currentAttempts;
				if(currentX > 0 && currentY > 0 && grid.tileContentAt(currentX - 1, currentY - 1) != Grid.TileContent.BlackPawn) {
					return new Move(currentX, currentY, currentX - 1, currentY - 1, false);
				}
			} 
			if(currentAttempts == 1) { //Try forward
				++currentAttempts;
				if(currentY > 0 && grid.tileContentAt(currentX, currentY - 1) == Grid.TileContent.Nothing) {
					return new Move(currentX, currentY, currentX, currentY - 1, false);
				}
			} 
			if(currentAttempts == 2){ //Try right diagonal
				++currentAttempts;
				if(currentX < 7 && currentY > 0 && grid.tileContentAt(currentX + 1, currentY - 1) != Grid.TileContent.BlackPawn) {
					return new Move(currentX, currentY, currentX + 1, currentY - 1, false);
				}
			}
			return nextValidMove();
		}
	}
	
	private void goNextTile() {
		currentAttempts = 0;
		if(currentX == 7) {
			currentX = 0;
			++currentY;
		} else {
			++currentX;
		}
	}
}
