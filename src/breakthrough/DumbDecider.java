package breakthrough;

public class DumbDecider implements IDecider {

	@Override
	public int ComputeScore(Grid grid, boolean maxPlayerWhite) {
		int whitePawns = grid.getPawnsCount(true);
		int blackPawns = grid.getPawnsCount(false);
		int difference = whitePawns - blackPawns;
		
		if(maxPlayerWhite) {
			return difference;
		} else {
			return -difference;
		}
	}

}
