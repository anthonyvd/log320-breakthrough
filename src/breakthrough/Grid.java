package breakthrough;

import java.util.Stack;


public class Grid {
	private static final long leftMostOne = 0x8000000000000000L;
	private static final long fullLeftOneLine = 0xFF00000000000000L;
	private static final long fullRightOneLine = 0x00000000000000FFL;

	public enum TileContent {
		WhitePawn, BlackPawn, Nothing,
	}
	
	private Stack<Long> moveStack = new Stack<Long>();
	private Stack<Integer> cachedCountsStack = new Stack<Integer>();
	
	private long whitePawns = 0L;
	private long blackPawns = 0L;
	
	private int cachedWhitePawnCount = 0;
	private int cachedBlackPawnCount = 0;
	
	public static Grid classicGrid() {
		Grid g = new Grid();
		g.whitePawns = 0xFFFF000000000000L;
		g.blackPawns = 0x000000000000FFFFL;
		return g;
	}
	
	public Grid() {
		
	}
	
	public Grid clone() {
		Grid g = new Grid();
		g.whitePawns = this.whitePawns;
		g.blackPawns = this.blackPawns;
		g.cachedWhitePawnCount = this.cachedWhitePawnCount;
		g.cachedBlackPawnCount = this.cachedBlackPawnCount;
		return g;
	}
	
	public Grid pushMove(Move move) {
		moveStack.push(whitePawns);
		moveStack.push(blackPawns);
		cachedCountsStack.push(this.cachedWhitePawnCount);
		cachedCountsStack.push(this.cachedBlackPawnCount);
		this.applyMove(move);
		return this;
	}
	
	public Grid popMove() {
		this.blackPawns = moveStack.pop();
		this.whitePawns = moveStack.pop();
		this.cachedBlackPawnCount = cachedCountsStack.pop();
		this.cachedWhitePawnCount = cachedCountsStack.pop();
		return this;
	}
	
	//TODO: cache pawn amounts
	public int getPawnsCount(boolean white) {
		if(white)
			return this.cachedWhitePawnCount;
		else
			return this.cachedBlackPawnCount;
	}
	
	public long getRawWhitePawns() {
		return this.whitePawns;
	}
	
	public long getRawBlackPawns() {
		return this.blackPawns;
	}
	
	private static int getShiftForCoordinates(int x, int y) {
		return 8 * y + x;
	}
	
	private void applyWhiteMove(Move move) {
		//Compute a mask containing a single 1 at the from and to board positions
		long fromMask = leftMostOne >>> getShiftForCoordinates(move.xStart, move.yStart);
		long toMask = leftMostOne >>> getShiftForCoordinates(move.xEnd, move.yEnd);
		
		if(this.tileContentAt(move.xEnd, move.yEnd) == TileContent.BlackPawn) {
			--this.cachedBlackPawnCount;
		}
		
		//Clear the "from" tile of whites
		whitePawns &= ~fromMask;
		//Adds the pawn at the "to" index
		whitePawns |= toMask;
		
		//Eats a black pawn already on the destination tile
		blackPawns &= ~toMask;
	}
	
	private void applyBlackMove(Move move) {
		//Compute a mask containing a single 1 at the from and to board positions
		long fromMask = leftMostOne >>> getShiftForCoordinates(move.xStart, move.yStart);
		long toMask = leftMostOne >>> getShiftForCoordinates(move.xEnd, move.yEnd);
		
		if(this.tileContentAt(move.xEnd, move.yEnd) == TileContent.WhitePawn) {
			--this.cachedWhitePawnCount;
		}
		
		//Clear the "from" tile of whites
		blackPawns &= ~fromMask;
		//Adds the pawn at the "to" index
		blackPawns |= toMask;
		
		//Eats a black pawn already on the destination tile
		whitePawns &= ~toMask;
	}
	
	//Applies a VALID move to the board.
	//Passing an invalid move here results in an undefined and invalid state.
	//Absolutely NO checks are performed.
	public Grid applyMove(Move move) {
		if(move.isWhite) {
			applyWhiteMove(move);
		} else {
			applyBlackMove(move);
		}
		return this;
	}
	
	public boolean victoryReached() {
		return whiteVictoryReached() || blackVictoryReached();
	}
	
	public boolean victoryReached(boolean white) {
		return white ? whiteVictoryReached() : blackVictoryReached();
	}
	
	public boolean whiteVictoryReached() {
		return (whitePawns & 0x00000000000000FFL) != 0L;
	}
	public boolean blackVictoryReached() {
		return (blackPawns & 0xFF00000000000000L) != 0L;
	}
	
	public int distanceFromVictory(boolean white) {
		long mask = white ? fullRightOneLine : fullLeftOneLine;
		for(int i = 0; i < 8; ++i) {
			if(white) {
				if((whitePawns & (mask << (i * 8))) != 0) return i;
			} else {
				if((blackPawns & (mask >>> (i * 8))) != 0) return i;
			}
		}
		return 7;
	}
	
	public int distanceFromHomeRow(boolean white) {
		long mask = white ? fullLeftOneLine : fullRightOneLine;
		for(int i = 0; i < 8; ++i) {
			if(white) {
				if((whitePawns & (mask >>> (i * 8))) != 0) return i;
			} else {
				if((blackPawns & (mask << (i * 8))) != 0) return i;
			}
		}
		return 7;
	}
	
	public TileContent tileContentAt(int x, int y) {
		int pos = getShiftForCoordinates(x, y);
		if((whitePawns & (leftMostOne >>> pos)) != 0L) {
			return TileContent.WhitePawn;
		} else if((blackPawns & (leftMostOne >>> pos)) != 0L) {
			return TileContent.BlackPawn;
		} else {
			return TileContent.Nothing;
		}
	}
	
	public boolean isValidMove(Move move) {
		if(!isInRange(move.xStart) || !isInRange(move.yStart) || !isInRange(move.xEnd) || !isInRange(move.yEnd)) {
			return false;
		}
		return move.isWhite ? isValidWhiteMove(move) : isValidBlackMove(move);
	}
	
	private boolean isValidWhiteMove(Move move) {
		if(move.xEnd - move.xStart > 1 || move.xEnd - move.xStart < -1) {
			return false;
		}
		if(move.yEnd - move.yStart != 1) {
			return false;
		}
		if(tileContentAt(move.xStart, move.yStart) != TileContent.WhitePawn) {
			return false;
		}
		if(move.xEnd - move.xStart == 0) {
			if(tileContentAt(move.xEnd, move.yEnd) != TileContent.Nothing) {
				return false;
			}
		} else {
			if(tileContentAt(move.xEnd, move.yEnd) == TileContent.WhitePawn) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isValidBlackMove(Move move) {
		if(move.xEnd - move.xStart > 1 || move.xEnd - move.xStart < -1) {
			return false;
		}
		if(move.yEnd - move.yStart != -1) {
			return false;
		}
		if(tileContentAt(move.xStart, move.yStart) != TileContent.BlackPawn) {
			return false;
		}
		if(move.xEnd - move.xStart == 0) {
			if(tileContentAt(move.xEnd, move.yEnd) != TileContent.Nothing) {
				return false;
			}
		} else {
			if(tileContentAt(move.xEnd, move.yEnd) == TileContent.BlackPawn) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isInRange(int coord) {
		return coord >= 0 && coord <=7;
	}
	
	private void putWhitePawn(int x, int y) {
		whitePawns |= leftMostOne >>> getShiftForCoordinates(x, y);
		blackPawns &= ~(leftMostOne >>> getShiftForCoordinates(x, y));
		
		++this.cachedWhitePawnCount;
	}
	
	private void putBlackPawn(int x, int y) {
		blackPawns |= leftMostOne >>> getShiftForCoordinates(x, y);
		whitePawns &= ~(leftMostOne >>> getShiftForCoordinates(x, y));
		++this.cachedBlackPawnCount;
	}
	
	public static Grid fromGridString(String str) {
		Grid g = new Grid();
		int strPos = 0;
		for(int j = 7; j >= 0; --j) {
			for(int i = 0; i < 8; ++i) {
				while(str.charAt(strPos) == ' ') {
					++strPos;
				}
				if(str.charAt(strPos) == '2') {
					g.putBlackPawn(i, j);
				} else if(str.charAt(strPos) == '4') {
					g.putWhitePawn(i, j);
				}
				++strPos;
			}
		}
		return g;
	}
	
	public void prettyPrint() {
		for(int j = 7; j >= 0; --j) {
			for(int i = 0; i < 8; ++i) {
				if(tileContentAt(i, j) == TileContent.BlackPawn) {
					System.out.print("X");
				} else if(tileContentAt(i, j) == TileContent.WhitePawn) {
					System.out.print("O");
				} else {
					System.out.print(".");
				}
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}
	
	public String oneLineString() {
		String str = "";
		for(int j = 7; j >= 0; --j) {
			for(int i = 0; i < 8; ++i) {
				if(tileContentAt(i, j) == TileContent.BlackPawn) {
					str += '2';
				} else if(tileContentAt(i, j) == TileContent.WhitePawn) {
					str += '4';
				} else {
					str += '0';
				}
				str += " ";
			}
		}
		return str;
	}
}
