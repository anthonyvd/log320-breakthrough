package breakthrough;


public class Breakthrough {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length > 0) {
			int index = 0;
			for(String s: args) {
				if(s.equals("-ai_key")) {
					if(args.length >= index + 2) {
						Options.aiKey = args[index + 1];
					}
				}
				if(s.equals("-server")) {
					if(args.length >= index + 2) {
						Options.serverHostname = args[index + 1];
					}
				}
				if(s.equals("-ai")) {
					if(args.length >= index + 2){
						Options.ai = args[index + 1];
					}
				}
				if(s.equals("-depth")) {
					if(args.length >= index + 2){
						Options.depth = Integer.parseInt(args[index + 1]);
					}
				}
				if(s.equals("-verbose")) {
					Options.verbose = true;
				}
				++index;
			}
		}
		
		if(Options.verbose) System.out.println("Trying to connect to server: " + Options.serverHostname + " with key: " + Options.aiKey);
		
		Communicator com = new Communicator();
		if(Options.ai.equals("HAL")){
			com.RegisterAI(new AlphaBetaPrunedMinMaxer(new HALDecider(), true, Options.depth));
		}
		else if(Options.ai.equals("Jarvis")){
			com.RegisterAI(new AlphaBetaPrunedMinMaxer(new JarvisDecider(), true, Options.depth));
		}else{
			if(Options.verbose) System.out.println("AI does not exist.");
		}
		
		if(!com.connectToServer(Options.serverHostname, Options.aiKey)) {
			if(Options.verbose) System.out.println("Unable to connect to server, aborting");
		} else {
			while(com.receiveMsg());
			if(Options.verbose) System.out.println("Aaaaaaaand, we're done!");
		}
	}
}
