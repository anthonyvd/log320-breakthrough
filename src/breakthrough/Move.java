package breakthrough;

public class Move {
	public int xStart;
	public int yStart;
	public int xEnd;
	public int yEnd;
	public boolean isWhite;
	
	public Move(int xStart, int yStart, int xEnd, int yEnd, boolean isWhite) {
		this.xStart = xStart;
		this.yStart = yStart;
		this.xEnd = xEnd;
		this.yEnd = yEnd;
		this.isWhite = isWhite;
	}
	
	private static int getIntFromLetter(char letter) {
		switch(letter) {
		case 'A':
			return 0;
		case 'B':
			return 1;
		case 'C':
			return 2;
		case 'D':
			return 3;
		case 'E':
			return 4;
		case 'F':
			return 5;
		case 'G':
			return 6;
		case 'H':
			return 7;
		default:
			return -1;
		}
	}
	
	private static char getCharFromInt(int i) {
		switch(i) {
		case 0:
			return 'A';
		case 1:
			return 'B';
		case 2:
			return 'C';
		case 3:
			return 'D';
		case 4:
			return 'E';
		case 5:
			return 'F';
		case 6:
			return 'G';
		case 7:
			return 'H';
		default:
			return '-';
		}
	}
	
	public static Move fromMoveString(String str, boolean isWhite) {
		if(str.length() == 7) {
			return fromDashedMoveString(str, isWhite);
		} else if(str.length() == 4) {
			return fromNonDashedMoveString(str, isWhite);
		}
		return null;
	}
	
	private static Move fromDashedMoveString(String str, boolean isWhite) {
		return new Move(
				getIntFromLetter(str.charAt(0)),
				Character.getNumericValue(str.charAt(1)) - 1,
				getIntFromLetter(str.charAt(5)),
				Character.getNumericValue(str.charAt(6)) - 1,
				isWhite);
	}
	
	private static Move fromNonDashedMoveString(String str, boolean isWhite) {
		return new Move(
				getIntFromLetter(str.charAt(0)),
				Character.getNumericValue(str.charAt(1)) - 1,
				getIntFromLetter(str.charAt(2)),
				Character.getNumericValue(str.charAt(3)) - 1,
				isWhite);
	}
	
	public String moveString() {
		return "" + getCharFromInt(xStart) + (yStart + 1) + getCharFromInt(xEnd) + (yEnd + 1);
	}
	
	public String moveStringWithDash() {
		return "" + getCharFromInt(xStart) + (yStart + 1) + " - " + getCharFromInt(xEnd) + (yEnd + 1);
	}
}
