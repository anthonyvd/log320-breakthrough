package breakthrough;

import java.util.concurrent.ArrayBlockingQueue;

public class PlayerQueue extends Thread {

	public ArrayBlockingQueue<RemotePlayer> playersInQueue = new ArrayBlockingQueue<RemotePlayer>(100);
	
	@Override
	public void run() {
		System.out.println("Starting game");
		RemotePlayer whitePlayer = null;
		RemotePlayer blackPlayer = null;
		
		try {
		while(true) {
				do {
					whitePlayer = playersInQueue.take();
					whitePlayer.aiId = DataInterface.validateAiKey(whitePlayer.readAiKey());
				} while(whitePlayer.aiId == -1);
				whitePlayer.white = true;
				
				do {
					blackPlayer = playersInQueue.take();
					blackPlayer.aiId = DataInterface.validateAiKey(blackPlayer.readAiKey());
				} while(blackPlayer.aiId == -1);
				blackPlayer.white = false;
				
				HostedGame newGame = new HostedGame(whitePlayer, blackPlayer);
				newGame.start();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
