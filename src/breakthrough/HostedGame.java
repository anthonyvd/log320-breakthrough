package breakthrough;

import java.util.ArrayList;
import java.util.List;

public class HostedGame extends Thread implements IRemotePlayerEventListener {
	RemotePlayer white;
	RemotePlayer black;
	Grid grid;
	
	private List<Move> movesPlayed = new ArrayList<Move>();
	
	public Iterable<Move> getMovesPlayed() {
		return movesPlayed;
	}
	
	public HostedGame(RemotePlayer white, RemotePlayer black) {
		grid = Grid.classicGrid();
		this.white = white;
		this.black = black;
	}
	
	void play() {
		System.out.println("Starting to play the game");
		if(!white.sendInitialGrid(grid) || !black.sendInitialGrid(grid)) {
			//Someone dropped before the game even started, bail
			return;
		}
		
		white.addListener(this);
		black.addListener(this);
		
		System.out.println("Sent initial grids");
		
		while(!grid.victoryReached()) {
			Move whiteMove = white.getMove();
			while(whiteMove == null || !grid.isValidMove(whiteMove)) {
				white.sendInvalidMove();
				whiteMove = white.getMove();
			}
			System.out.println("White played: " + whiteMove.moveString());
			grid.applyMove(whiteMove);
			black.sendMove(whiteMove);
			movesPlayed.add(whiteMove);
			if(grid.victoryReached(true)) {
				System.out.println("white won");
				end(true);
			} else {
				Move blackMove = black.getMove();
				while(blackMove == null || !grid.isValidMove(blackMove)) {
					black.sendInvalidMove();
					blackMove = black.getMove();
				}
				System.out.println("Black played: " + blackMove.moveString());
				grid.applyMove(blackMove);
				white.sendMove(blackMove);
				movesPlayed.add(blackMove);
				if(grid.victoryReached(false)) {
					System.out.println("black won");
					end(false);
				}
			}
		}
		System.out.println("Game over, thread dying");
	}
	
	void end(boolean winnerWhite) {
		if(winnerWhite) {
			white.sendVictory();
			black.sendDefeat();
			DataInterface.writeGameResultToDatabase(white.aiId, black.aiId, this);
		} else {
			black.sendVictory();
			white.sendDefeat();
			DataInterface.writeGameResultToDatabase(black.aiId, white.aiId, this);
		}
	}
	
	@Override
	public void run() {
		this.play();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void OnConnectionException(RemotePlayer player) {
		System.out.println("Player {ID: " + player.aiId + "} connection exception.");
		if(player == white) {
			end(false);
		} else {
			end(true);
		}
		this.stop();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void OnTimeout(RemotePlayer player) {
		System.out.println("Player {ID: " + player.aiId + "} timed out.");
		if(player == white) {
			end(false);
		} else {
			end(true);
		}
		this.stop();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void OnTooManyInvalidMoves(RemotePlayer player) {
		System.out.println("Player {ID: " + player.aiId + "} sent too many invalid moves.");
		if(player == white) {
			end(false);
		} else {
			end(true);
		}
		this.stop();
	}
}
