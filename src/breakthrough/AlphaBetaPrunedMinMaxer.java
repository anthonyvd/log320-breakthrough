package breakthrough;

import java.util.ArrayList;
import java.util.List;


public class AlphaBetaPrunedMinMaxer implements IArtificialIntelligence {

	private static long sendThreshold = (long)(4.5 * 1e9);
	
	private int maxDepth;
	private boolean white;
	private IDecider decider;
	private Grid internalGrid;
	
	List<SetDepthMinMaxer> threadedMinMaxers = new ArrayList<SetDepthMinMaxer>();
	
	public AlphaBetaPrunedMinMaxer(IDecider decider, boolean white, int maxDepth) {
		this.decider = decider;
		this.white = white;
		if(maxDepth % 2 == 1) {
			this.maxDepth = maxDepth;
		} else {
			this.maxDepth = maxDepth - 1;
		}
		
		for(int i = this.maxDepth; i > 0; i -= 2) {
			this.threadedMinMaxers.add(new SetDepthMinMaxer(i, this));
		}
	}
	
	/* (non-Javadoc)
	 * @see breakthrough.IArtificialIntelligence#OnInitialGridReceived(breakthrough.Grid, boolean)
	 */
	@Override
	public void OnInitialGridReceived(Grid grid, boolean opponentStarting, AbstractCommunicator sender) {
		this.white = !opponentStarting;
		this.internalGrid = grid;
		if(!opponentStarting) {
			Move m = chooseAndApplyMove();
			sender.sendMove(m);
			if(Options.verbose) System.out.println("We played: " + m.moveStringWithDash());
			if(Options.verbose) this.internalGrid.prettyPrint();
		}
	}

	/* (non-Javadoc)
	 * @see breakthrough.IArtificialIntelligence#OnMoveReceived(breakthrough.Move)
	 */
	@Override
	public void OnMoveReceived(Move opponentMove, AbstractCommunicator sender) {
		this.internalGrid.applyMove(opponentMove);
		
		if(Options.verbose) System.out.println("Opponent played: " + opponentMove.moveStringWithDash());
		this.internalGrid.prettyPrint();
		
		Move m = chooseAndApplyMove();
		sender.sendMove(m);
		
		if(Options.verbose) System.out.println("We played: " + m.moveStringWithDash());
		if(Options.verbose) this.internalGrid.prettyPrint();
	}
	
	private Move chooseAndApplyMove() {
		Move m = MinMaxAlphaBeta(this.internalGrid);
		this.internalGrid.applyMove(m);
		
		return m;
	}
	
	@SuppressWarnings("deprecation")
	public Move MinMaxAlphaBeta(Grid grid) {
		for(SetDepthMinMaxer minMaxer: this.threadedMinMaxers) {
			minMaxer.grid = grid.clone();
			minMaxer.start();
		}
		
		long time = System.nanoTime();
		
		while((System.nanoTime() - time) < sendThreshold);
		
		Move bestMove = null;
		
		for(SetDepthMinMaxer minMaxer: this.threadedMinMaxers) {
			if(minMaxer.move != null && bestMove == null) {
				if(Options.verbose) System.out.println("We went " + (minMaxer.depth + 1) + " levels deep.");
				bestMove = minMaxer.move;
			}
			minMaxer.stop();
		}
		
		this.threadedMinMaxers.clear();
		for(int i = this.maxDepth; i > 0; i -= 2) {
			this.threadedMinMaxers.add(new SetDepthMinMaxer(i, this));
		}
		
		return bestMove;
	}
	
	public Move firstMax(Grid g, int depth) {
		//generate all moves
		MoveGenerator mg = new MoveGenerator(g, this.white);
		int bestScore = Integer.MIN_VALUE;
		Move bestMove = null;
		
		//evaluate their scores
		Move next = null;
		while((next = mg.nextValidMove()) != null) {
			if(depth == 0) {
				int newScore = this.decider.ComputeScore(g.pushMove(next), this.white);
				//System.out.println("Move " + next.moveStringWithDash() + " has score " + newScore);
				if(bestScore <= newScore) {
					bestScore = newScore;
					bestMove = next;
				}
			} else {
				int newScore = internalMin(g.pushMove(next), depth - 1, bestScore);
				//System.out.println("Move " + next.moveStringWithDash() + " has score " + newScore);
				if(bestScore <= newScore) {
					bestScore = newScore;
					bestMove = next;
				}
			}
			g.popMove();
		}
		return bestMove;
	}
	
	public int internalMax(Grid g, int depth, int beta) {
		if(g.victoryReached(this.white)) {
			return Integer.MAX_VALUE;
		}
		if(g.victoryReached(!this.white)) {
			return Integer.MIN_VALUE;
		}
		
		//generate all moves
		MoveGenerator mg = new MoveGenerator(g, this.white);
		int bestScore = Integer.MIN_VALUE;
		
		//evaluate their scores
		Move next = null;
		int newScore = bestScore;
		while((next = mg.nextValidMove()) != null && bestScore <= beta) {
			if(depth == 0) {
				newScore = this.decider.ComputeScore(g.pushMove(next), this.white);
				if(bestScore < newScore) {
					bestScore = newScore;
				}
			} else {
				newScore = internalMin(g.pushMove(next), depth - 1, bestScore);
				if(bestScore < newScore) {
					bestScore = newScore;
				}
			}
			g.popMove();
		}
		return bestScore;
	}
			
	public int internalMin(Grid g, int depth, int alpha) {
		if(g.victoryReached(this.white)) {
			return Integer.MAX_VALUE;
		}
		if(g.victoryReached(!this.white)) {
			return Integer.MIN_VALUE;
		}
		
		//generate all moves
		MoveGenerator mg = new MoveGenerator(g, !this.white);
		int worstScore = Integer.MAX_VALUE;
		
		//evaluate their scores
		Move next = null;
		int newScore = worstScore;
		while((next = mg.nextValidMove()) != null && worstScore >= alpha) {
			if(depth == 0) {
				newScore = this.decider.ComputeScore(g.pushMove(next), this.white);
				if(worstScore > newScore) {
					worstScore = newScore;
				}
			} else {
				newScore = internalMax(g.pushMove(next), depth - 1, worstScore);
				if(worstScore > newScore) {
					worstScore = newScore;
				}
			}
			g.popMove();
		}
		return worstScore;
	}
}
