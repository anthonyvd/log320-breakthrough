package breakthrough;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class Communicator extends AbstractCommunicator {

	Socket socket = null;
	BufferedInputStream input = null;
	BufferedOutputStream output = null;
	BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
	byte[] moveBuffer = new byte[16];
	boolean opponentWhite;

	public Communicator() {
		
	}

	@Override
	public boolean connectToServer(String hostname, String aiKey) {
		try {
			socket = new Socket(hostname, 8888);
			input = new BufferedInputStream(socket.getInputStream());
			output = new BufferedOutputStream(socket.getOutputStream());
			if(!aiKey.equals("")) {
				output.write(aiKey.getBytes());
				output.flush();
			}
			return true;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean receiveMsg() {
		try {
			char c = (char) input.read();
			switch (c) {
			case '1':
				if(Options.verbose) System.out.println("On est les blanc et on joue live...");
				opponentWhite = false;
				receiveBoard();
				break;
			case '2':
				if(Options.verbose) System.out.println("On est les noir et on attend pour jouer");
				opponentWhite = true;
				receiveBoard();
				break;
			case '3':
				if(Options.verbose) System.out.println("C'est a nous de jouer");
				receiveMove();
				break;
			case '4':
				if(Options.verbose) System.out.println("On a fait un mouvement invalide");
				break;
			case '5':
				if(Options.verbose) System.out.println("On a gagne, sur le serveur");
				break;
			case '6':
				if(Options.verbose) System.out.println("On a perdu, sur le serveur");
				break;
			default:
				if(Options.verbose) System.out.println("Message not implemented, yet value of msg: " + c);
				break;
			}
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void receiveBoard() {
		try {
			byte[] aBuffer = new byte[1024];
			int size = input.available();
			input.read(aBuffer, 0, size);
			String s = new String(aBuffer).trim();
			if(Options.verbose) System.out.println("string: " + s); //grille sous le format d'une line
			this.notifyBoardReceived(Grid.fromGridString(s), opponentWhite);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void receiveMove() {
		try {
			int size;
			size = input.available();
			// System.out.println("size " + size);
			input.read(moveBuffer, 0, size);
			String s = new String(moveBuffer).trim();
			if(Options.verbose) System.out.println("Dernier coup: " + s);
			this.notifyMoveReceived(Move.fromMoveString(s, opponentWhite));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendMove(Move move) {
		String moveStr = move.moveString();
		if(Options.verbose) System.out.println("Sending Move " + moveStr);
		try {
			output.write(moveStr.getBytes());
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
