package breakthrough;

public interface IDecider {
	public abstract int ComputeScore(Grid grid, boolean maxPlayerWhite);
}
