package breakthrough;

public interface IRemotePlayerEventListener {
	public void OnConnectionException(RemotePlayer player);
	public void OnTimeout(RemotePlayer player);
	public void OnTooManyInvalidMoves(RemotePlayer player);
}
