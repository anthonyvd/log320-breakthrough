package breakthrough;

public class HALDecider implements IDecider {
	
	private static final int PAWN_DIFFERENCE_WEIGHT = 100000;
	
	@Override
	public int ComputeScore(Grid grid, boolean maxPlayerWhite) {
		if(grid.victoryReached(maxPlayerWhite)) {
			return Integer.MAX_VALUE;
		}
		if(grid.victoryReached(!maxPlayerWhite)) {
			return Integer.MIN_VALUE;
		}
		
		if(breakthrough(grid, maxPlayerWhite)) {
			return Integer.MAX_VALUE - grid.distanceFromVictory(maxPlayerWhite);
		}
		if(breakthrough(grid, !maxPlayerWhite)) {
			return Integer.MIN_VALUE + grid.distanceFromVictory(!maxPlayerWhite);
		}
		
		int ownPawns = grid.getPawnsCount(maxPlayerWhite);
		int opponentPawns = grid.getPawnsCount(!maxPlayerWhite);		
		int difference = ownPawns - opponentPawns;

		return  (difference * PAWN_DIFFERENCE_WEIGHT);
	}
	
	private boolean breakthrough(Grid grid, boolean white) {
		return grid.distanceFromVictory(white) <= grid.distanceFromHomeRow(!white);
	}
}
