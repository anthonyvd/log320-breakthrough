package breakthrough;

public class Tests {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grid g = Grid.fromGridString("2222222222222222000000000000000000000000000000004444444444444444");
		g.prettyPrint();
		
		AlphaBetaPrunedMinMaxer white = new AlphaBetaPrunedMinMaxer(new HALDecider(), true, 5);
		AlphaBetaPrunedMinMaxer black = new AlphaBetaPrunedMinMaxer(new HALDecider(), false, 5);
		
		int turnsPlayed = 0;
		
		while(!g.victoryReached()) {
			System.out.println(turnsPlayed);
			g.applyMove(white.MinMaxAlphaBeta(g));
			g.prettyPrint();
			g.applyMove(black.MinMaxAlphaBeta(g));
			g.prettyPrint();
			++turnsPlayed;
		}
	}

}
