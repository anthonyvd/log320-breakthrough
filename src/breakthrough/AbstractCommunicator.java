package breakthrough;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCommunicator {
	private List<IArtificialIntelligence> listAI = new ArrayList<IArtificialIntelligence>();
	
	public abstract boolean connectToServer(String hostname, String aiKey) throws UnknownHostException, IOException;
	public abstract void receiveMove();
	public abstract void sendMove(Move move);

	public void notifyMoveReceived(Move move) {
		for(IArtificialIntelligence ai : this.listAI) {
			ai.OnMoveReceived(move, this);
		}
	}
	
	public void notifyBoardReceived(Grid grid, boolean opponentWhite) {
		for(IArtificialIntelligence ai : this.listAI) {
			ai.OnInitialGridReceived(grid, opponentWhite, this);
		}
	}

	public void RegisterAI(IArtificialIntelligence ai) {
		this.listAI.add(ai);
	}
}