package breakthrough;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class RemotePlayer {
	private List<IRemotePlayerEventListener> listeners = new ArrayList<IRemotePlayerEventListener>();
	
	private static final long NANO_TIMEOUT = (long)(5 * 1e9);
	private static final int MAX_INVALID_MOVES = 3;
	
	Socket socket = null;
	BufferedInputStream input = null;
	BufferedOutputStream output = null;
	public boolean white;
	public int aiId = -1;
	public int invalidMoveCount = 0;
	
	public RemotePlayer(Socket socket, boolean white) {
		this.white = white;
		this.socket = socket;
		try {
			this.input = new BufferedInputStream(socket.getInputStream());
			this.output = new BufferedOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			RaiseConnectionException();
		}
	}
	
	public Move getMove() {
		try {
			if(availableOrTimeout()) {
				int size = input.available();
				if(size > 0) {
					byte[] moveBuffer = new byte[size];
					
					input.read(moveBuffer , 0, size);
					
					String s = new String(moveBuffer).trim();
					
					return Move.fromMoveString(s, white);
				}
			}
		} catch (IOException e) {
			RaiseConnectionException();
		}
		return null;
	}
	
	public void sendMove(Move move) {
		String str = "3 " + move.moveStringWithDash();
		
		try {
			this.output.write(str.getBytes());
			this.output.flush();
		} catch (IOException e) {
			RaiseConnectionException();
		}
	}
	
	public boolean sendInitialGrid(Grid grid) {
		String str = white ? "1 " : "2 ";
		str += grid.oneLineString();
		try {
			this.output.write(str.getBytes());
			this.output.flush();
		} catch (IOException e) {
			RaiseConnectionException();
			return false;
		}
		return true;
	}
	
	public void sendInvalidMove() {
		try {
			++this.invalidMoveCount;
			if(invalidMoveCount > MAX_INVALID_MOVES) {
				RaiseTooManyInvalidMoves();
			}
			this.output.write("4".getBytes());
			this.output.flush();
		} catch (IOException e) {
			RaiseConnectionException();
		}
	}
	
	public void sendVictory() {
		try {
			this.output.write("5".getBytes());
			this.output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendDefeat() {
		try {
			this.output.write("6".getBytes());
			this.output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String readAiKey() {
		try {
			if(availableOrTimeout()) {
				int size = input.available();
				if(size > 0) {
					byte[] buffer = new byte[size];
					input.read(buffer , 0, size);
					
					String s = new String(buffer).trim();
					
					return s;
				}
			}
		} catch (IOException e) {
			RaiseConnectionException();
		}
		return "";
	}
	
	private boolean availableOrTimeout() {
		long timeStarted = System.nanoTime();
		try {
			while(input.available() == 0) {
				if(System.nanoTime() - timeStarted > NANO_TIMEOUT) {
					RaiseTimeout();
					return false;
				}
			}
		} catch (IOException e) {
			RaiseConnectionException();
			return false;
		}
		return true;
	}
	
	private void RaiseConnectionException() {
		for(IRemotePlayerEventListener l: listeners) {
			l.OnConnectionException(this);
		}
	}
	
	private void RaiseTimeout() {
		for(IRemotePlayerEventListener l: listeners) {
			l.OnTimeout(this);
		}
	}
	
	private void RaiseTooManyInvalidMoves() {
		for(IRemotePlayerEventListener l: listeners) {
			l.OnTooManyInvalidMoves(this);	
		}
	}
	
	public void addListener(IRemotePlayerEventListener l) {
		listeners.add(l);
	}
}
