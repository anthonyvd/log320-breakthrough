package breakthrough;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private static final int port = 8888;
	public static void main(String[] argv) {
		if(Options.verbose) System.out.println("Starting server");
		PlayerQueue playerQueue = new PlayerQueue();
		playerQueue.start();
		try {
			ServerSocket listeningSocket = new ServerSocket(port);
			if(Options.verbose) System.out.println("Listening socket opened");
			Socket playerSocket;
			if(Options.verbose) System.out.println("Listening for peers on port: " + port);
			while(true) {
				playerSocket = listeningSocket.accept();
				if(Options.verbose) System.out.println("Received connection from address " + playerSocket.getInetAddress().toString());
				playerQueue.playersInQueue.put(new RemotePlayer(playerSocket, true));
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
