package breakthrough;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataInterface {
	
	private static Connection makeConnection() throws SQLException {
		return DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/breakthrough", 
				"breakthrough", 
				"log320-breakthrough-app");
	}
	
	static int validateAiKey(String key) {
		if(Options.verbose) System.out.println("Validating AI key: " + key);
		int result = -1;
		Connection connection = null;
		PreparedStatement select = null;
		ResultSet rs = null;
		try {
			connection = makeConnection();
			select = connection.prepareStatement("SELECT id FROM ais WHERE ai_key=?");
			select.setString(1, key);
			
			rs = select.executeQuery();
			if(rs.next()) {
				result = rs.getInt(1);
			} else {
				if(Options.verbose) System.out.println("Key: " + key + " was rejected");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) rs.close();
				if(select != null) select.close();
				if(connection != null) connection.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	static void writeGameResultToDatabase(int winnerId, int loserId, HostedGame game) {
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			connection = makeConnection();
			st = connection.prepareStatement("INSERT INTO games (winner, loser) VALUES(?, ?)", Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, winnerId);
			st.setInt(2, loserId);
			
			st.execute();
			rs = st.getGeneratedKeys();
			int gameId = -1;
			if(rs.next()) {
				gameId = rs.getInt(1);
			}
			
			if(gameId != -1) {
				writeGameMovesToDatabase(game.getMovesPlayed(), gameId);
			}
			
			if(winnerId != loserId) {
				updateRatingsInDatabase(winnerId, loserId, connection);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) rs.close();
				if(st != null) st.close();
				if(connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	static void writeGameMovesToDatabase(Iterable<Move> moves, int gameId) {
		Connection connection = null;
		PreparedStatement st = null;
		int moveIndex = 0;
		try {
			connection = makeConnection();
			connection.setAutoCommit(false);
			st = connection.prepareStatement("INSERT INTO game_moves (move, turn, game_id) VALUES (?, ?, ?)");
			for(Move m: moves) {
				st.setString(1, m.moveString());
				st.setInt(2, moveIndex);
				st.setInt(3, gameId);
				st.addBatch();
				++moveIndex;
			}
			st.executeBatch();
			connection.commit();
		} catch(SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				if(st != null) st.close();
				if(connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	static int getPlayerRating(int playerId, Connection connection) {
		String ratingFetch = "SELECT rating FROM ais WHERE id=?";
		PreparedStatement st = null;
		ResultSet rs = null;
		int rating = -1;
		try {
			st = connection.prepareStatement(ratingFetch);
			st.setInt(1, playerId);
			rs = st.executeQuery();
			
			if(rs.next()) {
				rating = rs.getInt(1);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) rs.close();
				if(st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rating;
	}
	
	static void updatePlayerRatingInDatabase(int playerId, int newRating, Connection connection) {
		String ratingUpdate = "UPDATE ais SET rating=? WHERE id=?";
		PreparedStatement st = null;
		
		try {
			st = connection.prepareStatement(ratingUpdate);
			st.setInt(1, newRating);
			st.setInt(2, playerId);
			st.execute();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) st.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	static void updateRatingsInDatabase(int winnerId, int loserId, Connection connection) {
		int winnerRating = -1;
		int loserRating = -1;
		int newWinnerRating = -1;
		int newLoserRating = -1;

		winnerRating = getPlayerRating(winnerId, connection);
		loserRating = getPlayerRating(loserId, connection);
		
		if(winnerRating >= 0 && loserRating >= 0) {
			newWinnerRating = calculateNewRating(winnerRating, loserRating, true, getKFactor(winnerId, connection));
			newLoserRating = calculateNewRating(loserRating, winnerRating, false, getKFactor(winnerId, connection));
			
			updatePlayerRatingInDatabase(winnerId, newWinnerRating, connection);
			updatePlayerRatingInDatabase(loserId, newLoserRating, connection);
		}
	}
	
	static int calculateNewRating(int oldRating, int opponentRating, boolean won, int kFactor) {
		double expectedScore = 1.0/(1.0 + Math.pow(10, ((double)(opponentRating - oldRating)) / 400.0));
		int change = (int)((double)kFactor * ((won ? 1.0 : 0.0) - expectedScore));
		if(oldRating + change >= 0)
			return oldRating + change;
		else
			return 0;
	}
	
	static int getKFactor(int playerId, Connection connection) {
		return 32;
	}
}
