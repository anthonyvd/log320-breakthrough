package breakthrough;

public class SetDepthMinMaxer extends Thread {
	public int depth = 0;
	private AlphaBetaPrunedMinMaxer ai;
	
	public Grid grid = null;
	public Move move = null;
	
	public SetDepthMinMaxer(int depth, AlphaBetaPrunedMinMaxer ai) {
		this.depth = depth;
		this.ai = ai;
	}
	
	@Override
	public void run() {
		move = null;
		move = ai.firstMax(grid, depth);
	}
}
